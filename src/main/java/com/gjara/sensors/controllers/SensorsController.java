package com.gjara.sensors.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.gjara.sensors.sensors.CarbonMonoxideSensor;
import com.gjara.sensors.sensors.NitrogenDioxideSensor;
import com.gjara.sensors.sensors.OzoneSensor;
import com.gjara.sensors.sensors.ParticulateMatterSensor;
import com.gjara.sensors.sensors.VolatileOrganicCompoundsSensor;
import org.springframework.web.bind.annotation.RequestParam;


@RestController
public class SensorsController {

    ParticulateMatterSensor pms = new ParticulateMatterSensor();
    CarbonMonoxideSensor cms = new CarbonMonoxideSensor();
    NitrogenDioxideSensor nds = new NitrogenDioxideSensor();
    OzoneSensor ozs = new OzoneSensor();
    VolatileOrganicCompoundsSensor vocs = new VolatileOrganicCompoundsSensor();

    @GetMapping("/pms")
    public String pms(){
        return pms.getData() + " µg/m³";
    }

    @GetMapping("/cms")
    public String cms() {
        return pms.getData() + " ppm (parts per million)";
    }

    @GetMapping("/nds")
    public String nds() {
        return pms.getData() + " µg/m³";
    }

    @GetMapping("/ozs")
    public String ozs() {
        return pms.getData() + " ppb (parts per billion)";
    }

    @GetMapping("/vocs")
    public String vocs() {
        return pms.getData() + " ppb (parts per billion)";
    }

}

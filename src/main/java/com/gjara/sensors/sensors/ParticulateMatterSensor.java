package com.gjara.sensors.sensors;

import java.util.Random;

public class ParticulateMatterSensor {
    
    int minValue;
    int maxValue;

    public ParticulateMatterSensor(){
        this.minValue = 0;
        this.maxValue = 1000;
    }

    public int getData(){
        return minValue + new Random().nextInt(maxValue - minValue + 1);
    }

}

package com.gjara.sensors.sensors;

import java.util.Random;

public class CarbonMonoxideSensor {

    int minValue;
    int maxValue;

    public CarbonMonoxideSensor(){
        this.minValue = 0;
        this.maxValue = 50;
    }

    public double getData(){
        int randomIntValue = minValue + new Random().nextInt(maxValue - minValue + 1);

        return randomIntValue;
    }
    
}

package com.gjara.sensors.sensors;

import java.util.Random;

public class NitrogenDioxideSensor {
    
    int minValue;
    int maxValue;

    public NitrogenDioxideSensor(){
        this.minValue = 0;
        this.maxValue = 200;
    }

    public int getData(){
        return minValue + new Random().nextInt(maxValue - minValue + 1);
    }

}

package com.gjara.sensors.sensors;

import java.util.Random;

public class OzoneSensor {
        
    int minValue;
    int maxValue;

    public OzoneSensor(){
        this.minValue = 0;
        this.maxValue = 200;
    }

    public int getData(){
        return minValue + new Random().nextInt(maxValue - minValue + 1);
    }

}

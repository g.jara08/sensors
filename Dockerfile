FROM openjdk:17

EXPOSE 8081

ADD target/sensors-0.0.1-SNAPSHOT.jar sensors.jar

ENTRYPOINT ["java", "-jar", "/sensors.jar"]
